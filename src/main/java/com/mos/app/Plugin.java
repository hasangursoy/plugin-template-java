package com.mos.app;

import java.lang.reflect.Method;

import com.mosteknoloji.app.App;
import com.mosteknoloji.app.Events;
import com.mosteknoloji.app.Node;

public class Plugin 
{
	private static String PLUGIN_NAME = "Clipboard"; //should be same as name of the main class of nodes
	
    public static void main( String[] args )
    {
    	try {
        	Class[] classes = Class.forName("com.mos.app."+PLUGIN_NAME).getClasses();
        	for (Class c : classes) {
        		Method init = c.getDeclaredMethod("Init");
            	init.invoke(null, null);
        	}
    	} catch (Exception e) {
    		for (Node.NodeFactory nf : App.nFactories) {
    			String guid = nf.Node.props != null ? nf.Node.props.guid : "";
    			Events.EmitError(guid, e.getMessage());
			}
		}
    	
    	App.main(null);
    }
}
