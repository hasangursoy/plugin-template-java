package com.mos.app;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mosteknoloji.app.Node;

public class Clipboard {
	private class Variable {
		public String scope;
		public String name;
	}
	
	public static class Get extends Node {
		private static String NodeName = "Plugin.Clipboard.Get";
		
		private static class NodeProps extends INodeProps {
			public Variable variable;
		}
		
		static void Init() {
			final Get aNode = new Get();
			aNode.OnCreateHandler = new OnCreateCallback() {
				public void OnCreate(byte[] data) throws Exception {
					try {
						aNode.OnCreateCB(data);
					} catch (Exception ex) {
						throw ex;
					}
				}
			};
			aNode.OnMessageHandler = new OnMessageCallback() {
				public byte[] OnMessage(byte[] data) throws Exception {
					try {
						return aNode.OnMessageCB(data); 
					} catch (Exception ex) {
						throw ex;
					}
				}
			};
			aNode.OnCloseHandler = new OnCloseCallback() {
				public void OnClose() throws Exception {
					try {
						aNode.OnCloseCB();
					} catch (Exception ex) {
						throw ex;
					}
				}
			};

			aNode.props = new NodeProps();
			new NodeFactory(aNode, NodeName);
		}
		
		private void OnCreateCB(byte[] data) throws Exception {
			
		}
		
		private byte[] OnMessageCB(byte[] data) throws Exception {
			String json = new String(data);
			JsonElement root = new JsonParser().parse(json);
			
			root.getAsJsonObject().addProperty("payload", "java plugin");
			return root.getAsJsonObject().toString().getBytes();
		}
		
		private void OnCloseCB() throws Exception {
			
		}
	}
}

